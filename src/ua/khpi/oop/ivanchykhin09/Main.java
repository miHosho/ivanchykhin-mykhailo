package ua.khpi.oop.ivanchykhin09;

import java.util.Arrays;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
	    JobVacancyContainer container = new JobVacancyContainer();
	    
	    JobVacancy vacancy1 = new JobVacancy("Java Developer", "Acme Inc.", "Full-time", 5000, null);
	    vacancy1.setTerms(Arrays.asList("Health insurance", "Gym membership"));
	    container.add(vacancy1);
	    
	    JobVacancy vacancy2 = new JobVacancy("Data Analyst", "Big Data Corp.", "Part-time", 3000, null);
	    vacancy2.setTerms(Arrays.asList("Flexible schedule", "Work from home"));
	    container.add(vacancy2);
	    
	    // get vacancy by index
	    JobVacancy vacancy = container.get(0);
	    System.out.println(vacancy.getName());
	    
	    // get all vacancies
	    List<JobVacancy> vacancies = container.getAll();
	    for (JobVacancy v : vacancies) {
	        System.out.println(v.getName());
	    }
	    
	    // remove vacancy
	    container.remove(vacancy);
	    
	    // check if container is empty
	    System.out.println(container.isEmpty());
	    
	    // clear container
	    container.clear();
	}
}