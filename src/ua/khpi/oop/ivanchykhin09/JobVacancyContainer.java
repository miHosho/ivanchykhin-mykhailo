package ua.khpi.oop.ivanchykhin09;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JobVacancyContainer implements Serializable {
    
    /**
	 * 
	 */
	// Class JobVacancyContainer:
// A container class for storing objects of type JobVacancy. 
public class JobVacancyContainer implements Serializable {
    
    private static final long serialVersionUID = 1L; // Serialization ID
    
    private List<JobVacancy> vacancies; // List of JobVacancy objects
    
    // Constructor initializes the list of vacancies
    public JobVacancyContainer() {
        vacancies = new ArrayList<>();
    }
    
    // Method to add a JobVacancy object to the container
    public void add(JobVacancy vacancy) {
        vacancies.add(vacancy);
    }
    
    // Method to remove a JobVacancy object from the container
    public void remove(JobVacancy vacancy) {
        vacancies.remove(vacancy);
    }
    
    // Method to get a JobVacancy object at a specified index
    public JobVacancy get(int index) {
        return vacancies.get(index);
    }
    
    // Method to get a copy of all the JobVacancy objects in the container
    public List<JobVacancy> getAll() {
        return new ArrayList<>(vacancies);
    }
    
    // Method to get the number of JobVacancy objects in the container
    public int size() {
        return vacancies.size();
    }
    
    // Method to check if the container is empty
    public boolean isEmpty() {
        return vacancies.isEmpty();
    }
    
    // Method to clear all JobVacancy objects from the container
    public void clear() {
        vacancies.clear();
    } 
}
    
    // other methods if needed
}
