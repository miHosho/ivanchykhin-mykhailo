package ua.khpi.oop.ivanchykhin15;

import java.io.Serializable;

public class JobVacancy implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String jobTitle;
    private String companyName;
    private String workingConditions;
    private double salary;
    private String specialty;
    private int experience;
    private String education;

    public JobVacancy(String jobTitle, String companyName, String workingConditions, double salary, String specialty, int experience, String education) {
        this.jobTitle = jobTitle;
        this.companyName = companyName;
        this.workingConditions = workingConditions;
        this.salary = salary;
        this.specialty = specialty;
        this.experience = experience;
        this.education = education;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWorkingConditions() {
        return workingConditions;
    }

    public void setWorkingConditions(String workingConditions) {
        this.workingConditions = workingConditions;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public String toString() {
        return "JobVacancy:" + "\n" +
                "jobTitle=" + jobTitle + '\n' +
                "companyName=" + companyName + '\n' +
                "workingConditions=" + workingConditions + '\n' +
                "salary=" + salary + "\n" +
                "specialty=" + specialty + '\n' +
                "experience=" + experience + "\n" +
                "education=" + education + '\n';
    }
    
}
