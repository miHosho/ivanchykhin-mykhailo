package ua.khpi.oop.ivanchykhin12;

import java.util.ArrayList;

public class JobVacancyContainer {
    private ArrayList<JobVacancy> jobVacancies;

    public JobVacancyContainer() {
        jobVacancies = new ArrayList<>();
    }

    public void addJobVacancy(JobVacancy jobVacancy) {
        jobVacancies.add(jobVacancy);
    }

    public void removeJobVacancy(JobVacancy jobVacancy) {
        jobVacancies.remove(jobVacancy);
    }

    public ArrayList<JobVacancy> getJobVacancies() {
        return jobVacancies;
    }
    public ArrayList<JobVacancy> filterBySalary(int minSalary, int maxSalary) {
        ArrayList<JobVacancy> filteredList = new ArrayList<JobVacancy>();
        for (JobVacancy jobVacancy : jobVacancies) {
            if (jobVacancy.getSalary() >= minSalary && jobVacancy.getSalary() <= maxSalary) {
                filteredList.add(jobVacancy);
            }
        }
        return filteredList;
    }
    public ArrayList<JobVacancy> filterByJobName(String jobName) {
        ArrayList<JobVacancy> filteredList = new ArrayList<JobVacancy>();
        for (JobVacancy jobVacancy : jobVacancies) {
            if (jobVacancy.getPosition() .equalsIgnoreCase(jobName)) {
                filteredList.add(jobVacancy);
            }
        }
        return filteredList;
    }


}
