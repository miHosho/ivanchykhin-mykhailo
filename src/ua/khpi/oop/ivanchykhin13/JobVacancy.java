package ua.khpi.oop.ivanchykhin12;




public class JobVacancy {
    private String position;
    private String company;
    private String workingConditions;
    private double salary;
    private int experience;

    private String[] requirements;

    public JobVacancy(String position, String company, String workingConditions, double salary, int experience, String[] requirements) {
        this.position = position;
        this.company = company;
        this.workingConditions = workingConditions;
        this.salary = salary;
        this.experience = experience;

        this.requirements = requirements;
    }

    public String getPosition() {
        return position;
    }

    public String getCompany() {
        return company;
    }

    public String getWorkingConditions() {
        return workingConditions;
    }

    public double getSalary() {
        return salary;
    }

    public double getExperience() {
        return experience;
    }

    public String[] getRequirements() {
        return requirements;
    }
    public void displayJobVacancy() {
		System.out.println("Job vacancy: " + position);
        System.out.println("Company: " + company);
        System.out.println("Employment type: " + workingConditions);
        System.out.println("Salary: " + salary + "$");
        System.out.println("Experience: " + experience + " year");

        System.out.println("Requirements: ");
        for (String requirement : requirements) {
            System.out.println("- " + requirement);
        }
    }

}
