package ua.khpi.oop.ivanchykhin12;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // Створюємо контейнер вакансій та додаємо до нього кілька вакансій
        JobVacancyContainer jobVacancyContainer = new JobVacancyContainer();
        jobVacancyContainer.addJobVacancy(new JobVacancy("Software Developer", "Google", "Full-time", 100000, 9,
                new String[] { "Computer Science", "3+ years experience", "Bachelor's degree" }));
        jobVacancyContainer.addJobVacancy(new JobVacancy("Data Analyst", "Facebook", "Part-time", 50000, 10, 
                new String[] { "Statistics", "1+ years experience", "Bachelor's degree" }));
        jobVacancyContainer.addJobVacancy(new JobVacancy("Product Manager", "Apple", "Full-time", 150000, 5,
                new String[] { "Business Administration", "5+ years experience", "Master's degree" }));
        jobVacancyContainer.addJobVacancy(new JobVacancy("Teacher", "School", "Full-time", 6500, 10,
                new String[] { "Business Administration", "5+ years experience", "English" }));

        ArrayList<JobVacancy> filteredJob = filterByJobNameInParallel(jobVacancyContainer, "Teacher");

        for (JobVacancy jobVacancy : filteredJob) {
            jobVacancy.displayJobVacancy();
            System.out.println();
        }
    }

    public static ArrayList<JobVacancy> filterByJobNameInParallel(JobVacancyContainer container, String jobName) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        List<Callable<ArrayList<JobVacancy>>> tasks = new ArrayList<>();
        tasks.add(() -> container.filterByJobName(jobName));
        tasks.add(() -> container.filterByJobName(jobName));
        tasks.add(() -> container.filterByJobName(jobName));

        List<Future<ArrayList<JobVacancy>>> results = executor.invokeAll(tasks);

        ArrayList<JobVacancy> filteredJob = new ArrayList<>();
        for (Future<ArrayList<JobVacancy>> result : results) {
            filteredJob.addAll(result.get());
        }

        executor.shutdown();

        return filteredJob;
    }
}
