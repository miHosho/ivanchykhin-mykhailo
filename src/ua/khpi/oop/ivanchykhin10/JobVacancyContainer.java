package ua.khpi.oop.ivanchykhin09;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JobVacancyContainer implements Serializable {
    
    /**
	 * 
	 */
	// Class JobVacancyContainer:
// A container class for storing objects of type JobVacancy. 
public class JobVacancyContainer implements Serializable {
    
    private static final long serialVersionUID = 1L; // Serialization ID
    
    private List<JobVacancy> vacancies; // List of JobVacancy objects
    
    // Constructor initializes the list of vacancies
    public JobVacancyContainer() {
        vacancies = new ArrayList<>();
    }
    
    // Method to add a JobVacancy object to the container
    public void add(JobVacancy vacancy) {
        vacancies.add(vacancy);
    }
    
    // Method to remove a JobVacancy object from the container
    public void remove(JobVacancy vacancy) {
        vacancies.remove(vacancy);
    }
    
    // Method to get a JobVacancy object at a specified index
    public JobVacancy get(int index) {
        return vacancies.get(index);
    }
    
    // Method to get a copy of all the JobVacancy objects in the container
    public List<JobVacancy> getAll() {
        return new ArrayList<>(vacancies);
    }
    
    // Method to get the number of JobVacancy objects in the container
    public int size() {
        return vacancies.size();
    }
    
    // Method to check if the container is empty
    public boolean isEmpty() {
        return vacancies.isEmpty();
    }
    
    // Method to clear all JobVacancy objects from the container
    public void clear() {
        vacancies.clear();
    } 
    // параметризований метод для сортування за назвою фірми
    public <T extends Comparable<T>> void sortByCompanyName(List<JobVacancy> list, boolean ascending) {
        list.sort((v1, v2) -> {
            int result = v1.getCompany().compareTo(v2.getCompany());
            return ascending ? result : -result;
        });
    }

    // параметризований метод для сортування за назвою спеціальності
    public <T extends Comparable<T>> void sortByJobTitle(List<T> vacancies, boolean ascending) {
    	list = null;
		list.sort((v1, v2) -> {
            int result = v1.getJobTitle().compareTo(v2.getJobTitle());
            return ascending ? result : -result;
        });
    }

    // параметризований метод для сортування за вказаною освітою
    public <T extends Comparable<T>> void sortByRequiredEducation(List<T> vacancies, boolean ascending) {
    	list.sort((v1, v2) -> {
            int result = v1.getRequiredEducation().compareTo(v2.getRequiredEducation());
            return ascending ? result : -result;
        });
}
    
    // other methods if needed
}
