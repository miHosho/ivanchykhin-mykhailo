package ua.khpi.oop.ivanchykhin12;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
        // Створюємо контейнер вакансій та додаємо до нього кілька вакансій
        JobVacancyContainer jobVacancyContainer = new JobVacancyContainer();
        jobVacancyContainer.addJobVacancy(new JobVacancy("Software Developer", "Google", "Full-time", 100000, 9,
                new String[] { "Computer Science", "3+ years experience", "Bachelor's degree" }));
        jobVacancyContainer.addJobVacancy(new JobVacancy("Data Analyst", "Facebook", "Part-time", 50000, 10, 
                new String[] { "Statistics", "1+ years experience", "Bachelor's degree" }));
        jobVacancyContainer.addJobVacancy(new JobVacancy("Product Manager", "Apple", "Full-time", 150000, 5,
                new String[] { "Business Administration", "5+ years experience", "Master's degree" }));
        jobVacancyContainer.addJobVacancy(new JobVacancy("Teacher", "School", "Full-time", 6500, 10,
                new String[] { "Business Administration", "5+ years experience", "English" }));

//        // Фільтруємо вакансії за зарплатною ставкою
//        ArrayList<JobVacancy> filteredList = jobVacancyContainer.filterBySalary(1000, 125000);
//
//        for (JobVacancy jobVacancy : filteredList) {
//            jobVacancy.displayJobVacancy();
//            System.out.println();
//        }
        
        ArrayList<JobVacancy> filteredJob = jobVacancyContainer.filterByJobName("Teacher");

        for (JobVacancy jobVacancy : filteredJob) {
            jobVacancy.displayJobVacancy();
            System.out.println();
        }
    }
}
