package ua.khpi.oop.ivanchykhin09;

import java.util.List;

public class JobVacancy {
    private String name;
    private String company;
    private String workingConditions;
    private int salary;
    private List<String> requirements;
	private List<String> terms;

    public JobVacancy(String name, String company, String workingConditions, int salary, List<String> requirements) {
        this.name = name;
        this.company = company;
        this.workingConditions = workingConditions;
        this.salary = salary;
        this.requirements = requirements;
    }

    public String getDescription() {
        String description = "Job vacancy: " + name +
                "\nCompany: " + company +
                "\nWorking Conditions: " + workingConditions +
                "\nSalary: " + salary +
                "\nRequirements: ";
        for (String requirement : requirements) {
            description += "\n- " + requirement;
        }
        return description;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWorkingConditions() {
        return workingConditions;
    }

    public void setWorkingConditions(String workingConditions) {
        this.workingConditions = workingConditions;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public List<String> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<String> requirements) {
        this.requirements = requirements;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }
}
