package ua.khpi.oop.ivanchykhin12;


import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;



public class MultiTest {

    @Test
    public void testAddJobVacancy() {
        JobVacancyContainer container = new JobVacancyContainer();
        JobVacancy jobVacancy = new JobVacancy("Software Developer", "ABC Company", 5000);
        container.addJobVacancy(jobVacancy);
        ArrayList<JobVacancy> jobVacancies = container.getJobVacancies();
        Assert.assertEquals(1, jobVacancies.size());
        Assert.assertTrue(jobVacancies.contains(jobVacancy));
    }

    @Test
    public void testRemoveJobVacancy() {
        JobVacancyContainer container = new JobVacancyContainer();
        JobVacancy jobVacancy = new JobVacancy("Software Developer", "ABC Company", 5000);
        container.addJobVacancy(jobVacancy);
        container.removeJobVacancy(jobVacancy);
        ArrayList<JobVacancy> jobVacancies = container.getJobVacancies();
        Assert.assertEquals(0, jobVacancies.size());
        Assert.assertFalse(jobVacancies.contains(jobVacancy));
    }

    @Test
    public void testFilterBySalary() {
        JobVacancyContainer container = new JobVacancyContainer();
        JobVacancy jobVacancy1 = new JobVacancy("Software Developer", "ABC Company", 5000);
        JobVacancy jobVacancy2 = new JobVacancy("Database Administrator", "XYZ Inc", 8000);
        container.addJobVacancy(jobVacancy1);
        container.addJobVacancy(jobVacancy2);
        ArrayList<JobVacancy> filteredList = container.filterBySalary(5000, 7000);
        Assert.assertEquals(1, filteredList.size());
        Assert.assertTrue(filteredList.contains(jobVacancy1));
    }

    @Test
    public void testFilterByJobName() {
        JobVacancyContainer container = new JobVacancyContainer();
        JobVacancy jobVacancy1 = new JobVacancy("Software Developer", "ABC Company", 5000);
        JobVacancy jobVacancy2 = new JobVacancy("Database Administrator", "XYZ Inc", 8000);
        container.addJobVacancy(jobVacancy1);
        container.addJobVacancy(jobVacancy2);
        ArrayList<JobVacancy> filteredList = container.filterByJobName("software developer");
        Assert.assertEquals(1, filteredList.size());
        Assert.assertTrue(filteredList.contains(jobVacancy1));
    }
}
