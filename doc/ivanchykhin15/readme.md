#15. Колекції в Java∗
## Тема
- Ознайомлення з бібліотекою колекцій Java SE.
- Використання колекцій для розміщення об'єктів розроблених класів.



## 1. Вимоги
Розробити консольну програму для реалізації завдання обробки даних згідно прикладної області.

Для розміщення та обробки даних використовувати контейнери (колекції) і алгоритми з Java Collections Framework.

Забезпечити обробку колекції об'єктів: додавання, видалення, пошук, сортування згідно розділу Прикладні задачі л.р. №10.

Передбачити можливість довготривалого зберігання даних: 1) за допомогою стандартної серіалізації; 2) не використовуючи протокол серіалізації.

Продемонструвати розроблену функціональність в діалоговому та автоматичному режимах за результатом обробки параметрів командного рядка.




### 1.1 Розробник
- Іванчихін М.Ю.
- КН-921в
- 2 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
див 1

## 2. Вивід у консоль
~~~bash
    JobVacancy:
    jobTitle=Java Developer
    companyName=Google
    workingConditions=Flexible
    salary=100000.0
    specialty=Computer Science
    experience=3
    education=Bachelor's

    JobVacancy:
    jobTitle=Software Engineer
    companyName=Microsoft
    workingConditions=Remote
    salary=120000.0
    specialty=Computer Science
    experience=5
    education=Master's

    JobVacancy:
    jobTitle=Data Analyst
    companyName=Amazon
    workingConditions=On-site
    salary=80000.0
    specialty=Statistics
    experience=2
    education=Bachelor's

    Job vacancies at Google: 
    JobVacancy:
    jobTitle=Java Developer
    companyName=Google
    workingConditions=Flexible
    salary=100000.0
    specialty=Computer Science
    experience=3
    education=Bachelor's

    Job vacancies after removal: 
    JobVacancy:
    jobTitle=Java Developer
    companyName=Google
    workingConditions=Flexible
    salary=100000.0
    specialty=Computer Science
    experience=3
    education=Bachelor's

    JobVacancy:
    jobTitle=Data Analyst
    companyName=Amazon
    workingConditions=On-site
    salary=80000.0
    specialty=Statistics
    experience=2
    education=Bachelor's

    Job vacancies at Google, sorted by specialty: 
    JobVacancy:
    jobTitle=Java Developer
    companyName=Google
    workingConditions=Flexible
    salary=100000.0
    specialty=Computer Science
    experience=3
    education=Bachelor's

    Serialized data is saved in vacancies.ser
    Deserialized vacancies: 
    JobVacancy:
    jobTitle=Java Developer
    companyName=Google
    workingConditions=Flexible
    salary=100000.0
    specialty=Computer Science
    experience=3
    education=Bachelor's

    JobVacancy:
    jobTitle=Data Analyst
    companyName=Amazon
    workingConditions=On-site
    salary=80000.0
    specialty=Statistics
    experience=2
    education=Bachelor's

~~~

### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main

### 2.3 Важливі фрагменти програми:
 
~~~java
        startTime = System.currentTimeMillis();
        List<Integer> parallelResult = processInParallel(list);
        endTime = System.currentTimeMillis();
        long parallelTime = endTime - startTime;
~~~

~~~java
 private static List<Integer> generateRandomList(int numElements) {
        Random rand = new Random();
        List<Integer> list = new ArrayList<>(numElements);
        for (int i = 0; i < numElements; i++) {
            list.add(rand.nextInt(1000));
        }
        return list;
    }
~~~
~~~java
 private static List<Integer> processSequentially(List<Integer> list) {
        List<Integer> result = new ArrayList<>(list.size());
        for (Integer i : list) {
            sleep(5);
            result.add(i * 2);
        }
        return result;
    }
~~~


~~~java
    private static List<Integer> processInParallel(List<Integer> list) {
        List<Integer> result = new ArrayList<>(list.size());
        list.parallelStream().forEach(i -> {
            sleep(5);
            result.add(i * 2);
        });
        return result;
    }
~~~
## Варіанти використання
Демонстрація роботи  потоків
## Висновки
На цій лабораторній роботі навчились працювати з потоками