# 10. Обробка параметризованих контейнерів
## Тема
- Розширення функціональності параметризованих класів.


## 1. Вимоги
1. Використовуючи програму рішення завдання лабораторної роботи №9:

2. Продемонструвати розроблену функціональність (створення, управління та обробку власних контейнерів) в діалоговому та автоматичному режимах.

2.1 Автоматичний режим виконання програми задається параметром командного рядка -auto. Наприклад, java ClassName -auto.

2.2 В автоматичному режимі діалог з користувачем відсутній, необхідні данні генеруються, або зчитуються з файлу.

3. Забороняється використання алгоритмів з Java Collections Framework.

### 1.1 Розробник
- Іванчихін М.Ю.
- КН-921в
- 2 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
Кадрове агентство. Сортування за назвою фірми, за назвою запропонованої спеціальності, за вказаною освітою

## 2. Вивід у консоль
відсутній
### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. JobVacancy
- 3. JobVacancyContainer

### 2.3 Важливі фрагменти програми:
- 
~~~java
// параметризований метод для сортування за назвою фірми
    public <T extends Comparable<T>> void sortByCompanyName(List<JobVacancy> list, boolean ascending) {
        list.sort((v1, v2) -> {
            int result = v1.getCompany().compareTo(v2.getCompany());
            return ascending ? result : -result;
        });
    }

    // параметризований метод для сортування за назвою спеціальності
    public <T extends Comparable<T>> void sortByJobTitle(List<T> vacancies, boolean ascending) {
    	list = null;
		list.sort((v1, v2) -> {
            int result = v1.getJobTitle().compareTo(v2.getJobTitle());
            return ascending ? result : -result;
        });
    }

    // параметризований метод для сортування за вказаною освітою
    public <T extends Comparable<T>> void sortByRequiredEducation(List<T> vacancies, boolean ascending) {
    	list.sort((v1, v2) -> {
            int result = v1.getRequiredEducation().compareTo(v2.getRequiredEducation());
            return ascending ? result : -result;
        });
    }
~~~

## Варіанти використання
Демонстрація роботи параметризованих контейнерів 
## Висновки
На цій лабораторній роботі навчились працювати з параметризованими контейнерами
 