#14. Multithreading. Ефективність використання
## Тема
- Вимірювання часу паралельних та послідовних обчислень.
- Демонстрація ефективності паралельної обробки.



## 1. Вимоги
1. Забезпечити вимірювання часу паралельної обробки елементів контейнера за допомогою розроблених раніше методів.




2. Додати до алгоритмів штучну затримку виконання для кожної ітерації циклів поелементної обробки контейнерів, щоб загальний час обробки був декілька секунд.


3. Реалізувати послідовну обробку контейнера за допомогою методів, що використовувались для паралельної обробки та забезпечити вимірювання часу їх роботи.

4. Порівняти час паралельної і послідовної обробки та зробити висновки про ефективність розпаралелювання:

- результати вимірювання часу звести в таблицю;
- обчислити та продемонструвати у скільки разів паралельне виконання швидше послідовного.





### 1.1 Розробник
- Іванчихін М.Ю.
- КН-921в
- 2 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
див 1

## 2. Вивід у консоль

<img src="img/result.jpg">

### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. JobVacancy
- 3. JobVacancyContainer

### 2.3 Важливі фрагменти програми:
 
~~~java
public static void main(String[] args) {
        // Створюємо колекцію об'єктів
        List<JobVacancy> vacancies = new ArrayList<>();
        
        // Додаємо елементи до колекції
        vacancies.add(new JobVacancy("Java Developer", "Google", "Flexible", 100000, "Computer Science", 3, "Bachelor's"));
        vacancies.add(new JobVacancy("Software Engineer", "Microsoft", "Remote", 120000, "Computer Science", 5, "Master's"));
        vacancies.add(new JobVacancy("Data Analyst", "Amazon", "On-site", 80000, "Statistics", 2, "Bachelor's"));

        // Створюємо об'єкт контейнеру та додаємо до нього колекцію об'єктів
        JobVacancyContainer container = new JobVacancyContainer();
        container.addAll(vacancies);

        // Виводимо елементи контейнеру у консоль
        container.printAll();

        // Пошук вакансії за назвою фірми
        String companyName = "Google";
        List<JobVacancy> vacanciesByCompany = container.findByCompanyName(companyName);
        System.out.println("Job vacancies at " + companyName + ": ");
        for (JobVacancy vacancy : vacanciesByCompany) {
            System.out.println(vacancy);
        }

        // Видалення вакансії з контейнеру
        container.remove(vacancies.get(1));
        System.out.println("Job vacancies after removal: ");
        container.printAll();

        List<JobVacancy> result = container.findByCompanyName("Google");
        Collections.sort(result, new SpecialtyComparator());
        
        System.out.println("Job vacancies at Google, sorted by specialty: ");
        for (JobVacancy vacancy : result) {
            System.out.println(vacancy);
        }

        // Серіалізація контейнеру до файлу
        container.serialize("vacancies.ser");

        // Десеріалізація контейнеру з файлу
        JobVacancyContainer deserializedContainer = JobVacancyContainer.deserialize("vacancies.ser");
        System.out.println("Deserialized vacancies: ");
        deserializedContainer.printAll();
}
~~~

## Варіанти використання
Демонстрація роботи колекцій
## Висновки
На цій лабораторній роботі навчились працювати з колекціями