# 9. Параметризація в Java
## Тема
- Вивчення принципів параметризації в Java.
- Розробка параметризованих класів та методів.


## 1. Вимоги
1. Створити власний клас-контейнер, що параметризується (Generic Type), на основі зв'язних списків для реалізації колекції domain-об’єктів лабораторної роботи №7.

2. Для розроблених класів-контейнерів забезпечити можливість використання їх об'єктів у циклі foreach в якості джерела даних.

3. Забезпечити можливість збереження та відновлення колекції об'єктів: 
  3.1 за допомогою стандартної серіалізації;
  3.2 не використовуючи протокол серіалізації.

4. Продемонструвати розроблену функціональність: створення контейнера, додавання елементів, видалення елементів, очищення контейнера, перетворення у масив, перетворення у рядок, перевірку на наявність елементів

5. Забороняється використання контейнерів (колекцій) з Java Collections Framework.


### 1.1 Розробник
- Іванчихін М.Ю.
- КН-921в
- 2 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
див. у 3

## 2. Вивід у консоль
~~~bash
Java Developer
Java Developer
Data Analyst
false

~~~
<img src="img/result.jpg">

### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. JobVacancy
- 3. JobVacancyContainer

### 2.3 Важливі фрагменти програми:
- 
~~~java
// Class JobVacancyContainer:
// A container class for storing objects of type JobVacancy. 
public class JobVacancyContainer implements Serializable {
    
    private static final long serialVersionUID = 1L; // Serialization ID
    
    private List<JobVacancy> vacancies; // List of JobVacancy objects
    
    // Constructor initializes the list of vacancies
    public JobVacancyContainer() {
        vacancies = new ArrayList<>();
    }
    
    // Method to add a JobVacancy object to the container
    public void add(JobVacancy vacancy) {
        vacancies.add(vacancy);
    }
    
    // Method to remove a JobVacancy object from the container
    public void remove(JobVacancy vacancy) {
        vacancies.remove(vacancy);
    }
    
    // Method to get a JobVacancy object at a specified index
    public JobVacancy get(int index) {
        return vacancies.get(index);
    }
    
    // Method to get a copy of all the JobVacancy objects in the container
    public List<JobVacancy> getAll() {
        return new ArrayList<>(vacancies);
    }
    
    // Method to get the number of JobVacancy objects in the container
    public int size() {
        return vacancies.size();
    }
    
    // Method to check if the container is empty
    public boolean isEmpty() {
        return vacancies.isEmpty();
    }
    
    // Method to clear all JobVacancy objects from the container
    public void clear() {
        vacancies.clear();
    } 
}
~~~

## Варіанти використання
Демонстрація роботи параметризації 
## Висновки
На цій лабораторній роботі навчились працювати з параметризацією