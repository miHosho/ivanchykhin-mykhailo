# 12. Регулярні вирази. Обробка тексту∗
## Тема
- Ознайомлення з принципами використання регулярних виразів для обробки тексту.



## 1. Вимоги
1. Використовуючи програми рішень попередніх задач, продемонструвати ефективне (оптимальне) використання регулярних виразів при вирішенні прикладної задачі.


2. Передбачити можливість незначної зміни умов пошуку.

3. Продемонструвати розроблену функціональність в діалоговому та автоматичному режимах.




### 1.1 Розробник
- Іванчихін М.Ю.
- КН-921в
- 2 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
Кадрове агентство. Знайти всі вакансії, де потрібні викладачі (педагоги, вчителі) зі стажем не менше 10 років, які знають англійську мову та володіють автомобілем.


## 2. Вивід у консоль

<img src="img/result.jpg">

### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main
- 1. FileValidator

### 2.3 Важливі фрагменти програми:
 
~~~java
     public static ArrayList<JobVacancy> filterByJobNameInParallel(JobVacancyContainer container, String jobName) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        List<Callable<ArrayList<JobVacancy>>> tasks = new ArrayList<>();
        tasks.add(() -> container.filterByJobName(jobName));
        tasks.add(() -> container.filterByJobName(jobName));
        tasks.add(() -> container.filterByJobName(jobName));

        List<Future<ArrayList<JobVacancy>>> results = executor.invokeAll(tasks);

        ArrayList<JobVacancy> filteredJob = new ArrayList<>();
        for (Future<ArrayList<JobVacancy>> result : results) {
            filteredJob.addAll(result.get());
        }

        executor.shutdown();

        return filteredJob;
    }
~~~

## Варіанти використання
Демонстрація роботи потоків 
## Висновки
На цій лабораторній роботі навчились працювати з потоками